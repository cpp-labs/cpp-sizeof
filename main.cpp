#include <iostream>

struct EmptyStruct {};

struct NonEmptyStruct {
    int x;
    int y;
};

struct PointerStruct {
    int *x;
    int *y;
};

int main() {
    std::cout << "Primitive Types:" << std::endl;
    std::cout << "\tInteger - " << sizeof(int) << " byte(s)" << std::endl;
    std::cout << "\tCharacter - " << sizeof(char) << " byte(s)" << std::endl;
    std::cout << "\tBoolean - " << sizeof(bool) << " byte(s)" << std::endl;
    std::cout << "\tFloat - " << sizeof(float) << " byte(s)" << std::endl;
    std::cout << "\tDouble - " << sizeof(double) << " byte(s)" << std::endl;
    std::cout << "\tWide Char - " << sizeof(wchar_t) << " byte(s)" << std::endl;


    std::cout << "Structure and pointers:" << std::endl;
    std::cout << "\tEmpty struct - " << sizeof(EmptyStruct) << " byte(s)" << std::endl;
    std::cout << "\tNon-Empty struct - " << sizeof(NonEmptyStruct) << " byte(s)" << std::endl;

    NonEmptyStruct nes = {2, 1};
    std::cout << "\tPointer to Non-Empty struct - " << sizeof(&nes) << " byte(s)" << std::endl;
    std::cout << "\tStruct with pointers - " << sizeof(PointerStruct) << " byte(s)" << std::endl;

    return 0;
}
